import React, {PropTypes} from 'react';
import Button from 'react-bootstrap/lib/Button';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';
import ImageManager from './ImageManager.jsx';
import ActionCreator from '../actions/ImageActionCreators';

export default React.createClass({
  propTypes: {
    images: PropTypes.array.isRequired,
    onClear: PropTypes.func.isRequired,
    onAddImage: PropTypes.func.isRequired

  },

  getDefaultProps() {
    return {
      images: []
    }
  },

  render() {
    let {onAddImage, onClear, images} = this.props;
    return (
      <div className="container">
        <Jumbotron>
          <h1>Image Manager</h1>

        </Jumbotron>

        <ImageManager onAddImage={onAddImage} images={images} />

        {/* <Button onClick={onClear} bsStyle="danger">Clear List</Button>*/}
      </div>
    );
  }
});

import React from 'react';
import {ListGroup,ListGroupItem,Panel} from 'react-bootstrap/lib/ListGroup';
import Button from 'react-bootstrap/lib/Button';


export default React.createClass({
  getDefaultProps() {
    return {
      images: []
    };
  },

  handleSubmit(e) {
      let image = {};
      image.imageurl = this.refs.imageurl.getDOMNode().value;
      image.imagescore = this.refs.imagescore.getDOMNode().value;
      //this.props.images.push(image); NOOOOOOOO!!!
      this.props.onAddImage(image.imageurl,image.imagescore);
      e.preventDefault();
      return false; // this is not really needed the e.preventDefault(); is enough
  },

  render() {
    let {images} = this.props;

    let listimages =
        images.map((image,index)=> <tr>
          <td >
          {image.url}
          </td>
          <td>
            {image.score}
          </td>
          <td>
            <img src={image.url} width="150px"></img>
          </td>
          </tr>
        );


    return (<div>
        <form  onSubmit={this.handleSubmit.bind(this)}>
            <div className="form-group">
              <label htmlFor="imageurl">Image URL:</label>
              <input className="form-control" id="imageurl" ref="imageurl" type="url"/>

              <label htmlFor="imagescore">Image Score:</label>
              <input className="form-control" id="imagescore" ref="imagescore" type="number"/>
            </div>
            <div className="form-group">
              <Button type="submit" bsStyle="primary">Add</Button>
            </div>
      </form>
      <br></br>
      <hr></hr>
      <div className="panel panel-default">
          {images.length} images
            <div className="panel-body">
              <table className="table table-striped">
               <thead>
                 <tr>
                   <th>Image URL</th>
                   <th>Score</th>
                   <th>Image</th>
                 </tr>
               </thead>
               <tbody>

                {listimages.length?
                   {listimages}:''
                  }
                </tbody></table>
          </div>
        </div>

    </div>
  );

  }
});

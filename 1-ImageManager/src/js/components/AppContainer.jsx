import React from 'react';
  import ImageStore from '../stores/ImageStore';
import ActionCreator from '../actions/ImageActionCreators';
import App from './App.jsx';

export default React.createClass({
  _onChange() {
    this.setState(ImageStore.getAll());
  },

  getInitialState() {
    return ImageStore.getAll();
  },

  componentDidMount() {
    ImageStore.addChangeListener(this._onChange);
  },

  componentWillUnmount() {
    ImageStore.removeChangeListener(this._onChange);
  },

  handleAddImage(url,score) {

    if (url) {
      ActionCreator.addImage(url,score);
    }
  },

  handleClear(e) {
    ActionCreator.clearList();
  },

  render() {
    let {images} = this.state;
    return (
      <App
        onAddImage={this.handleAddImage}
        onClear={this.handleClear}
        images={images} />
    );
  }
});

import React from 'react';
import ActionCreator from '../actions/TodoActionCreators';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Input from 'react-bootstrap/lib/Input';

export default React.createClass({
  getDefaultProps() {
    return {
      task: {
        title: '',
        completed: false
      }
    };
  },

  handleToggle(task) {
    // (LM==> 13=) this will check the reference to the control (ref)
    //        and if it is, starts an update flow with the ActionCreator
    if (this.refs.checkbox.getChecked()) {
      ActionCreator.completeTask(task);
    }
  },

  render() {
    let {task} = this.props;
    return (
      <ListGroupItem>
        {/* (LM==> 11=) we have to bind the handleToggle since on call time its this will not be this this. */}
        <Input type="checkbox" ref="checkbox" checked={task.completed}
                    onChange={this.handleToggle.bind(this, task)} label={task.title} />
      </ListGroupItem>
    );
  }
});

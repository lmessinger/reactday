import React from 'react';
import navigate from 'page'
import ImageStore from '../stores/ImageStore';
import ActionCreator from '../actions/ImageActionCreators';
import {Router} from './Router.jsx';


export default React.createClass({
  _onChange() {
    this.setState(ImageStore.getAll());
  },

  getInitialState() {

    return ImageStore.getAll();
  },

  componentDidMount() {
    ActionCreator.loadImages();
    ImageStore.addChangeListener(this._onChange);
  },

  componentWillUnmount() {
    ImageStore.removeChangeListener(this._onChange);
  },

  handleAddImage(url,score) {

    if (url) {
      ActionCreator.addImage(url,score);
    }
  },

  handleClear(e) {
    ActionCreator.clearList();
  },

  render() {
    let {images} = this.state;
    return (
      <div>
        <nav className="navbar navbar-default">
            <div className="navbar-header">
              <a className="navbar-brand" href="#">Image</a>
            </div>
            <div>
              <ul className="nav navbar-nav">
                <li><a href='/imagemanager'>Manager</a></li>
                <li><a href='/imageslider'>Slider</a></li>
              </ul>
            </div>
        </nav>
        <Router onAddImage={this.handleAddImage}
          onClear={this.handleClear}
          images={images}>

        </Router>
      </div>

    );
  }
});

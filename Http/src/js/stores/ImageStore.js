import Dispatcher from '../Dispatcher';
import Constants from '../Constants';
import BaseStore from './BaseStore';
import assign from 'object-assign';
import $ from 'jquery';

const URL = "http://localhost:8080/images.json";
// data storage
let _data = [];
function loadStore(cb) {
  $.ajax({
      url: URL,
      dataType: 'json',
      cache: false,
      success: function(data) {
        cb (data);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(URL, status, err.toString());
        cb([]);
      }.bind(this)
    });
}


// add private functions to modify data
function addImage(url, score = -1) {
  _data = _data.concat({url, score});
  console.log(score,'sss');
}

// Facebook style store creation.
const ImageStore = assign({}, BaseStore, {
  // public methods used by Controller-View to operate on data
  getAll() {
    return {
      images: _data
    };
  },

  loadImages(cb) {
    loadStore(function(data){
      console.log('doddata',data);
      _data = data.images;
      cb();
    });

  },
  // register store with dispatcher, allowing actions to flow through
  dispatcherIndex: Dispatcher.register(function handleAction(payload) {
    const action = payload.action;

    switch (action.type) {
    case Constants.ActionTypes.IMAGE_ADDED:
      const url = action.url.trim();
      // NOTE: if this action needs to wait on another store:
      // Dispatcher.waitFor([OtherStore.dispatchToken]);
      // For details, see: http://facebook.github.io/react/blog/2014/07/30/flux-actions-and-the-dispatcher.html#why-we-need-a-dispatcher
      if (url !== '') {
        console.log(action.score)
        addImage(url,action.score);
        ImageStore.emitChange();
      }
      break;

    // add more cases for other actionTypes...
    case Constants.ActionTypes.IMAGE_LOAD:
    {
      //const url = action.url.trim();
      // NOTE: if this action needs to wait on another store:
      // Dispatcher.waitFor([OtherStore.dispatchToken]);
      // For details, see: http://facebook.github.io/react/blog/2014/07/30/flux-actions-and-the-dispatcher.html#why-we-need-a-dispatcher
      //if (url !== '') {
        ImageStore.loadImages(function() {
            ImageStore.emitChange();
        });

      }
      break;
    // no default
    }
  })
});

export default ImageStore;

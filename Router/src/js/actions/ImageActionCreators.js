import Dispatcher from '../Dispatcher';
import Constants from '../Constants';

/* eslint-disable no-console */

export default {
  addImage(url,score=-1) {
    Dispatcher.handleViewAction({
      type: Constants.ActionTypes.IMAGE_ADDED,
      url: url,
      score: score
    });
  },

  clearList() {
    console.warn('clearList action not yet implemented...');
  },

  completeTask(task) {
    console.warn('completeTask action not yet implemented...', task);
  }
};

import {ImageManagerPage} from '../pages/ImageManagerPage.jsx';
import {ImageSliderPage} from '../pages/ImageSliderPage.jsx';
import React from 'react';
import page from 'page'

export class Router extends React.Component {
  // Operations usually carried out in componentWillMount go here
  constructor(props) {
    super(props);
    this.state = {
      component: 'div'
    }

  }

  componentDidMount() {
    debugger;
    page('/',  (ctx) =>
        this.setState({ component: ImageManagerPage })
      );

    page('/imagemanager', (ctx) =>
             this.setState({ component: ImageManagerPage })
      );

    page('/imageslider', (ctx) =>
             this.setState({ component: ImageSliderPage })
      );

      page('*', (ctx) =>
               this.setState({ component: ImageSliderPage })
        );

    page.start();
  }

  render() {
    let re = React.createElement(this.state.component,this.props);
    return re;
  }
}

Router.defaultProps = {};

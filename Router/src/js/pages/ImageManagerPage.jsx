import React, {PropTypes} from 'react';
import Button from 'react-bootstrap/lib/Button';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';
import ImageManager from '../components/ImageManager.jsx';
import ActionCreator from '../actions/ImageActionCreators';

export class ImageManagerPage extends React.Component {

  render() {
    console.log('image page render');
    let {onAddImage, onClear, images} = this.props;
    return <div className="container">
        <Jumbotron>
        <h1>Image Manager</h1>  </Jumbotron>

        <ImageManager onAddImage={onAddImage} images={images} />

        {/* <Button onClick={onClear} bsStyle="danger">Clear List</Button>*/}
      </div>
  }
}
